from unittest import mock

import pytest
import requests


@pytest.fixture
def access_token_request_server(requests_mock):
    with requests.Session() as mock_session:
        with mock.patch("requests.Session", autospec=True, return_value=mock_session):
            requests_mock.register_uri(
                "POST", "https://authorization-server.com/oauth/access_token"
            )
            mock_session.mount("https://", requests_mock._adapter)
            mock_session.mount = lambda prefix, adapter: None

            yield


@pytest.fixture(params=[400, 401, 403, 404])
def access_token_request_http_error_server(request, requests_mock):
    with requests.Session() as mock_session:
        with mock.patch("requests.Session", autospec=True, return_value=mock_session):
            requests_mock.register_uri(
                "POST",
                "https://authorization-server.com/oauth/access_token",
                status_code=request.param,
            )
            mock_session.mount("https://", requests_mock._adapter)
            mock_session.mount = lambda prefix, adapter: None

            yield


@pytest.fixture
def access_token_request_error_server(requests_mock):
    with requests.Session() as mock_session:
        with mock.patch("requests.Session", autospec=True, return_value=mock_session):
            requests_mock.register_uri(
                "POST",
                "https://authorization-server.com/oauth/access_token",
                exc=requests.RequestException,
            )
            mock_session.mount("https://", requests_mock._adapter)
            mock_session.mount = lambda prefix, adapter: None

            yield
