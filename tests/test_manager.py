import json
from urllib.parse import parse_qsl, urlparse

import pytest
import requests

from auth_code_flow import FlowManager
from auth_code_flow.exceptions import AuthCodeFlowError

flow_manager = FlowManager(
    base_uri="https://authorization-server.com/oauth",
    client_id="a client id",
    client_secret="a client secret",
    redirect_uri="https://client-server.com/callback",
    scope="read_api write_api",
)


def test_get_authorization_endpoint():
    url = flow_manager.get_authorization_endpoint(state="a state")

    # the url path
    parts = urlparse(url)
    assert (
        parts.scheme + "://" + parts.netloc + parts.path
        == "https://authorization-server.com/oauth/authorize"
    )
    # the query parameters
    query_params = dict(parse_qsl(parts.query))
    assert query_params["client_id"] == "a client id"
    assert query_params["redirect_uri"] == "https://client-server.com/callback"
    assert query_params["response_type"] == "code"
    assert query_params["scope"] == "read_api write_api"
    assert query_params["state"] == "a state"


def test_get_access_token_endpoint():
    url = flow_manager.get_access_token_endpoint()

    assert url == "https://authorization-server.com/oauth/access_token"


@pytest.mark.usefixtures("access_token_request_server")
def test_fetch_access_token():
    resp = flow_manager.fetch_access_token(code="a mock code", state="a mock state")

    assert resp.status_code == 200
    # data was posted as a json dict in a byte string
    posted_params = json.loads(resp.request.body)
    assert posted_params["client_id"] == "a client id"
    assert posted_params["client_secret"] == "a client secret"
    assert posted_params["code"] == "a mock code"
    assert posted_params["grant_type"] == "authorization_code"
    assert posted_params["redirect_uri"] == "https://client-server.com/callback"
    assert posted_params["state"] == "a mock state"


@pytest.mark.usefixtures("access_token_request_server")
def test_fetch_access_token_for_payload_posted_as_form_data():
    resp = flow_manager.fetch_access_token(
        code="a mock code", state="a mock state", post_form_data=True
    )

    assert resp.status_code == 200
    # data was posted as a url-encoded query (byte) string
    posted_params = dict(parse_qsl(resp.request.body))
    assert posted_params["client_id"] == "a client id"
    assert posted_params["client_secret"] == "a client secret"
    assert posted_params["code"] == "a mock code"
    assert posted_params["grant_type"] == "authorization_code"
    assert posted_params["redirect_uri"] == "https://client-server.com/callback"
    assert posted_params["state"] == "a mock state"


@pytest.mark.usefixtures("access_token_request_error_server")
@pytest.mark.parametrize("post_form_data", [True, False])
def test_fetch_access_token_fails_for_request_exception(post_form_data):
    with pytest.raises(AuthCodeFlowError) as excinfo:
        flow_manager.fetch_access_token(
            code="a mock code", state="a mock state", post_form_data=post_form_data
        )

    exception = excinfo.value
    assert exception.response is None


@pytest.mark.usefixtures("access_token_request_http_error_server")
@pytest.mark.parametrize("post_form_data", [True, False])
def test_fetch_access_token_fails_for_http_error(post_form_data):
    with pytest.raises(AuthCodeFlowError) as excinfo:
        flow_manager.fetch_access_token(
            code="a mock code", state="a mock state", post_form_data=post_form_data
        )

    exception = excinfo.value
    assert isinstance(exception.response, requests.Response)
